import { addTeachersToMain } from "./teachers.js";
import { getFilteredUsers } from "../functions/users-filter.js";
import { countriesWithoutDuplicates } from "../data/countries.js";

export function addFilters() {
  //filters for age
  const ageFilter = document.getElementById("age-filter");
  ageFilter.addEventListener("change", async function () {
    const filteredUsers = await getFilteredUsers(
      null,
      ageFilter.value,
      null,
      null,
      null
    );
    console.log(filteredUsers);
    addTeachersToMain(filteredUsers);
  });

  //filter for country
  const addCountryFilterOptions = () => {
    countriesWithoutDuplicates.forEach((country) => {
      addCountryFilter(country);
    });
  };

  const addCountryFilter = (country) => {
    // function addCountryFilter(country)
    const countryFilter = document.getElementById("country-filter");

    const countryElement = document.createElement("option");

    countryElement.innerHTML = `<option value="${country}">${country}</option>`;

    countryFilter.appendChild(countryElement);
  };

  addCountryFilterOptions();

  const countryFilter = document.getElementById("country-filter");
  countryFilter.addEventListener("change", async function () {
    const filteredUsers = await getFilteredUsers(
      countryFilter.value,
      null,
      null,
      null,
      null
    );
    addTeachersToMain(filteredUsers);
  });

  //filter for user's sex
  const sexFilter = document.getElementById("sex-filter");
  sexFilter.addEventListener("change", async function () {
    const filteredUsers = await getFilteredUsers(
      null,
      null,
      sexFilter.value,
      null,
      null
    );
    addTeachersToMain(filteredUsers);
  });

  //filter for favorites
  const onlyFavoritesFilter = document.getElementById("favorites");
  onlyFavoritesFilter.addEventListener("change", async function () {
    const filteredUsers = await getFilteredUsers(
      null,
      null,
      null,
      onlyFavoritesFilter.checked,
      null
    );
    addTeachersToMain(filteredUsers);
  });

  //filter for with photo

  const onlyWithPictureFilter = document.getElementById("photos");
  onlyWithPictureFilter.addEventListener("change", async function () {
    const filteredUsers = await getFilteredUsers(
      null,
      null,
      null,
      null,
      onlyWithPictureFilter.checked
    );
    addTeachersToMain(filteredUsers);
  });
}
