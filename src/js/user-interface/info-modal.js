import { getUserByParameter } from "../functions/user-search.js";
import { addMap } from "./map.js";

// import dayjs from ".glob-DtwhSM7Udayjs";

function daysUntilNextBirthday(birthDate) {
  const today = dayjs();
  const birthday = dayjs(birthDate);

  // Calculate the next birthday for this year
  const nextBirthdayThisYear = birthday.year(today.year());

  // Calculate the next birthday for next year
  const nextBirthdayNextYear = birthday.year(today.year() + 1);

  // Calculate the difference in days
  const differenceThisYear = nextBirthdayThisYear.diff(today, "day");
  const differenceNextYear = nextBirthdayNextYear.diff(today, "day");

  // If the next birthday for this year has already passed, return days till next year's birthday
  if (differenceThisYear < 0) {
    return differenceNextYear;
  }

  return differenceThisYear;
}
export function addInfoModalToPage() {
  const teacherElements = document.getElementsByClassName("teacher-info");

  const infoModal = document.getElementById("info-modal");

  // Iterate through the teacher elements
  for (const teacher of teacherElements) {
    teacher.addEventListener("click", async () => {
      map.style.display = "none";

      const name = teacher.querySelector(".teachers-name").textContent;

      const teacherData = await getUserByParameter("full_name", name); //get user's info

      //change info in modal
      //name
      const currentTeacherName = document.getElementById("info-modal-name");
      currentTeacherName.innerText = teacherData.full_name;

      //specialization
      const currentTeacherSpecialization = document.getElementById(
        "info-modal-specialization"
      );
      currentTeacherSpecialization.innerText = teacherData.course;

      //country
      const currentTeacherCityCountry = document.getElementById(
        "info-modal-city-country"
      );
      currentTeacherCityCountry.innerText = `${teacherData.city}, ${teacherData.country}`;

      //phone
      const currentTeacherPhone = document.getElementById("info-modal-phone");
      currentTeacherPhone.innerText = teacherData.phone;

      //email
      const currentTeacherEmail = document.getElementById("info-modal-email");
      currentTeacherEmail.innerText = teacherData.email;

      //note
      const currentTeacherNote = document.getElementById("info-modal-note");
      currentTeacherNote.innerText = teacherData.note;

      //photo
      const currentTeacherPhoto = document.getElementById("info-modal-photo");
      currentTeacherPhoto.src = teacherData.picture_large;
      currentTeacherPhoto.alt = teacherData.full_name;

      //age& gender info-modal-age-gender
      const currentTeacherAge_Gender = document.getElementById(
        "info-modal-age-gender"
      );
      currentTeacherAge_Gender.innerText = `${teacherData.age}, ${teacherData.gender}`;

      //days to birth
      const daysToBirthElement = document.getElementById(
        "info-modal-days-to-birth"
      );
      daysToBirthElement.innerText = `birthday is in ${daysUntilNextBirthday(
        teacherData.b_date
      )} days`;

      infoModal.style.display = "block";

      const openMap = document.getElementById("info-modal-map");
      openMap.addEventListener("click", async () => {
        infoModal.style.display = "none";
        console.log(teacherData);
        scrollToTop();

        const map = document.getElementById("map");
        map.style.display = "block";
        await addMap(
          teacherData.coordinates.latitude,
          teacherData.coordinates.longitude
        );
      });
    });
  }

  // Call the closeModal function
  closeModal();
}

function closeModal() {
  const closeButton = document.getElementById("info-close-modal-window");
  closeButton.addEventListener("click", function () {
    const infoModal = document.getElementById("info-modal");
    infoModal.style.display = "none";
  });
}

function scrollToTop() {
  window.scrollTo({ top: 0, behavior: "smooth" });
}
