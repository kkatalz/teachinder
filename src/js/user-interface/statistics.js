import { additionalUsers, randomUserMock } from "../data/FE4U-Lab2-mock.js";
import { convertRandomUserMock } from "../functions/users-convert.js";
import { getSortedUsers } from "../functions/users-sorted.js";
import { getUsersFromServerToStatistics } from "../services/users.js";
let users = convertRandomUserMock(randomUserMock, additionalUsers);

const statistics = document.getElementById("customers");

export function addUsersToStatistics(usersParam = users) {
  if (statistics) {
    // to remove all elements except of header tr
    while (statistics.children.length > 1) {
      statistics.removeChild(statistics.children[1]);
    }
  }
  usersParam.forEach((user) => {
    addStatisticsUser(
      user.full_name,
      user.course,
      user.age,
      user.gender,
      user.country
    );
  });
}
function addStatisticsUser(name, speciality, age, gender, nationality) {
  const newTeacher = document.createElement("tr");
  newTeacher.innerHTML = `<tr>
<td>${name}</td>
<td>${speciality}</td>
<td>${age}</td>
<td>${gender}</td>
<td>${nationality}</td>
</tr>`;

  statistics.appendChild(newTeacher);
}

//sorting
export async function addStatistics() {
  let currentPage = 1;

  function addSortEventListener(elementId, sortBy) {
    const element = document.getElementById(elementId);
    let ascending = true;

    element.addEventListener("click", async () => {
      const sortOrder = ascending ? "asc" : "desc";
      const sortedUsers = await getSortedUsers(sortBy, sortOrder, currentPage);
      addUsersToStatistics(sortedUsers);
      ascending = !ascending;
    });
  }

  addSortEventListener("sort-name", "full_name");
  addSortEventListener("sort-speciality", "course");
  addSortEventListener("sort-age", "age");
  addSortEventListener("sort-gender", "gender");
  addSortEventListener("sort-nationality", "country");

  const loadMoreButton = document.getElementById("load-more");

  loadMoreButton.addEventListener("click", async () => {
    currentPage++;
    await getUsersFromServerToStatistics(`&results=${currentPage * 10}`);
  });
}
