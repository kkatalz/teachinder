import { getUsersFromServerToStatistics } from "../services/users.js";

const map = L.map("map");

export async function addMap(latitude, longitude) {
  map.setView([latitude, longitude], 12); // Set initial coordinates and zoom level
  const teachers = await getUsersFromServerToStatistics(`&results=50`, true);

  // Create a map centered at a specific location

  // Add a tile layer (OpenStreetMap)
  L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
    maxZoom: 4,
  }).addTo(map);

  function onMarkerClick(e) {
    // ZOOM: Set view to marker's position with zoom level 4
    map.setView(e.target.getLatLng(), 4);
  }

  // Iterate through the teachers and add markers
  teachers.forEach((teacher) => {
    const { coordinates, full_name } = teacher;

    //instead of
    //  const coordinates = teacher.coordinates;
    // const latitude = coordinates.latitude;

    if (!coordinates) return;
    const { latitude, longitude } = coordinates;
    // Create a marker
    L.marker([latitude, longitude])
      .addTo(map)
      .bindPopup(full_name) // Set the teacher's full name as the popup content
      .on("click", onMarkerClick);
  });
}
