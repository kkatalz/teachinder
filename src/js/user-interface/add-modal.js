import { countriesWithoutDuplicates } from "../data/countries.js";
import { courses } from "../data/courses.js";
import { getYearsFromDate } from "../functions/users-convert.js";
import { addUserToLocalServer } from "../services/users.js";

export function addModalToPage() {
  const addElementToSelect = (selectId, optionValue) => {
    const selectElement = document.getElementById(selectId);
    const optionElement = document.createElement("option");
    optionElement.value = optionValue;
    optionElement.textContent = optionValue;
    selectElement.appendChild(optionElement);
  };

  const addCountryFilterOptions = () => {
    _.forEach(countriesWithoutDuplicates, (country) => {
      addElementToSelect("add-modal-country", country);
    });
  };

  const addCourseFilterOptions = () => {
    _.forEach(courses, (course) => {
      addElementToSelect("add-modal-speciality", course);
    });
  };

  addCountryFilterOptions();
  addCourseFilterOptions();

  function addModal() {
    const addTeacherButtons = document.querySelectorAll(".add-teacher-button");

    _.forEach(addTeacherButtons, function (button) {
      button.addEventListener("click", function () {
        console.log("clicked");
        const addModal = document.getElementById("add-modal");
        addModal.style.display = "block";
      });
    });
  }

  addModal();

  function closeModal() {
    const closeButton = document.getElementById("add-modal-close");
    closeButton.addEventListener("click", function () {
      const addModal = document.getElementById("add-modal");
      addModal.style.display = "none";
    });
  }

  function addUser() {
    const modalForm = document.getElementById("add-modal-form");

    modalForm.addEventListener("submit", async function (e) {
      e.preventDefault();
      const addModal = document.getElementById("add-modal");
      const name = document.getElementById("add-modal-name").value;
      const course = document.getElementById("add-modal-speciality").value;
      const country = document.getElementById("add-modal-country").value;
      const city = document.getElementById("add-modal-city").value;
      const email = document.getElementById("add-modal-email").value;
      const phone = document.getElementById("add-modal-phone").value;
      const date = document.getElementById("add-modal-date").value;
      const male = document.getElementById("add-modal-male");
      const female = document.getElementById("add-modal-female");
      const bgColor = document.getElementById("add-modal-bg-color").value;
      const notes = document.getElementById("add-modal-notes").value;

      const newUser = {
        gender: male.checked ? "male" : female.checked ? "female" : null,
        title: null,
        full_name: name || null,
        city: city || null,
        state: null,
        country: country || null,
        postcode: null,
        coordinates: null,
        timezone: null,
        email: email || null,
        b_date: date || null,
        age: getYearsFromDate(date) || null,
        phone: phone || null,
        picture_large: null,
        picture_thumbnail: null,
        id: null,
        favorite: false,
        course: course || null,
        bg_color: bgColor || null,
        note: notes || null,
      };
      await addUserToLocalServer(newUser);

      addModal.style.display = "none";
    });
  }

  addUser();
  closeModal();
}
