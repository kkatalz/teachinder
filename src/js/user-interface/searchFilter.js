import { getUserByParameter } from "../functions/user-search.js";
import { addTeachersToMain } from "./teachers.js";

export function searchUserThroughInput() {
  const inputSearchFilter = document.getElementById("search-input-filter");
  const searchForm = document.getElementById("search-form");
  // const inputButtonFilter = document.getElementById("search-button-filter");
  searchForm.addEventListener("submit", async (event) => {
    event.preventDefault();
    const searchedUserByFullName = await getUserByParameter(
      "full_name",
      inputSearchFilter.value
    );
    const searchedUserByAge = await getUserByParameter(
      "age",
      inputSearchFilter.value
    );
    const searchedUserByNote = await getUserByParameter(
      "note",
      inputSearchFilter.value
    );
    const searchedUsers = [
      searchedUserByFullName || searchedUserByAge || searchedUserByNote,
    ];

    addTeachersToMain(searchedUsers);
  });
}
