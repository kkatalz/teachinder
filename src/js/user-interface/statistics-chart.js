import {
  getUsersFromServer,
  getUsersFromServerToStatistics,
} from "../services/users.js";

function createPieChart(data, label, chartTitle, canvasId) {
  const counts = data.reduce((counts, item) => {
    counts[item] = (counts[item] || 0) + 1;
    return counts;
  }, {});

  const labels = Object.keys(counts);
  const chartData = Object.values(counts);

  const ctx = document.getElementById(canvasId).getContext("2d");
  new Chart(ctx, {
    type: "pie",
    data: {
      labels: labels,
      datasets: [
        {
          data: chartData,
          backgroundColor: [
            "rgba(255, 99, 132, 0.7)",
            "rgba(54, 162, 235, 0.7)",
            "rgba(255, 206, 86, 0.7)",
            "rgba(75, 192, 192, 0.7)",
            "rgba(153, 102, 255, 0.7)",
            "rgba(255, 159, 64, 0.7)",
          ],
        },
      ],
    },
    options: {
      title: {
        display: true,
        text: chartTitle,
      },
      layout: {
        padding: {
          bottom: 20, // Increase this value to adjust the space for the legend
        },
      },
      cutoutPercentage: 50, // Adjust this value to change the size of the pie chart

      plugins: {
        legend: {
          position: "bottom",
        },
      },
    },
  });
}

export async function addStatisticsChart() {
  const teachers = await getUsersFromServerToStatistics(`&results=50`, true);
  // Chart 1: Gender
  createPieChart(
    _.map(teachers, "gender"),
    "gender",
    "Teacher Genders",
    "chart1"
  );

  // Chart 2: Speciality (course)
  createPieChart(
    _.map(teachers, "course"),
    "course",
    "Teacher Specialities",
    "chart2"
  );

  // Chart 3: Age
  createPieChart(_.map(teachers, "age"), "age", "Teacher Ages", "chart3");

  // Chart 4: Nationality
  createPieChart(
    _.map(teachers, "country"),
    "country",
    "Teacher Nationalities",
    "chart4"
  );
}
