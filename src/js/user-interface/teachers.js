import { convertRandomUserMock } from "../functions/users-convert.js";
import { additionalUsers, randomUserMock } from "../data/FE4U-Lab2-mock.js";
import { getUsersFromServer } from "../services/users.js";

let users = convertRandomUserMock(randomUserMock, additionalUsers);

export function addTeachersToMain(usersParam = users) {
  // by default using users
  const teachersContainer = document.getElementById("mainTeachers-for-js");
  teachersContainer.innerHTML = "";

  usersParam.forEach((user) => {
    const newTeacher = getNewTeacher(
      user.full_name,
      user.course,
      user.country,
      user.picture_large,
      user.favorite
    );

    teachersContainer.appendChild(newTeacher);
  });
}

function getNewTeacher(name, course, country, picture, favorite) {
  const newTeacher = document.createElement("div");

  newTeacher.innerHTML = `
          <div class="teacher-info">
            <div class="img-add-container">
              <div class="img-container">
              ${
                picture
                  ? `<img
              src="${picture}"
              alt="${name}"
              class="teachers-photo"
            />`
                  : `<div class="teachers-photo">${name}</div>`
              }
                
              </div>
              ${
                //conditional rendering
                favorite
                  ? '<img src="images/star.png" alt="star" class="teachers-star" />'
                  : ""
              }
            </div>
            <h2 class="teachers-name">${name || ""}</h2>
            <h1 class="teachers-specialization">${course || ""}</h1>
            <h1 class="teachers-country">${country || ""}</h1>
          </div>`;

  return newTeacher;
}

export function addTeachersToFavorites(users) {
  const teachersContainer = document.getElementById("favoritesTeachers-for-js");
  const filteredUsers = users.filter((user) => user.favorite);

  filteredUsers.forEach((user) => {
    teachersContainer.appendChild(
      getNewTeacher(
        user.full_name,
        user.course,
        user.country,
        user.picture_large,
        user.favorite
      )
    );
  });
}
