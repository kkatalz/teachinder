import {
  getUsersFromLocalServer,
  getUsersFromServer,
  getUsersFromServerToStatistics,
} from "./services/users.js";
import { addModalToPage } from "./user-interface/add-modal.js";
import { addFilters } from "./user-interface/filters.js";
import { addMap } from "./user-interface/map.js";
import { searchUserThroughInput } from "./user-interface/searchFilter.js";
import { addStatisticsChart } from "./user-interface/statistics-chart.js";
import { addStatistics } from "./user-interface/statistics.js";

getUsersFromServer();
// getUsersFromServerToStatistics("&results=10");
addFilters();
// addStatistics();
addStatisticsChart();
searchUserThroughInput();
addModalToPage();

getUsersFromLocalServer();
