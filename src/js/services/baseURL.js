export const baseURL = {
  external: "https://randomuser.me/api?seed=abc", // seed=abc - return the same users
  local: "http://localhost:3000/users",
};
