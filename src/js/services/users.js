import { convertRandomUserMock } from "../functions/users-convert.js";
import { addInfoModalToPage } from "../user-interface/info-modal.js";
import { addMap } from "../user-interface/map.js";
import { addUsersToStatistics } from "../user-interface/statistics.js";
import {
  addTeachersToFavorites,
  addTeachersToMain,
} from "../user-interface/teachers.js";
import { baseURL } from "./baseURL.js";

//integration of backend
//show users in Main from offered url
export async function getUsersFromServer(url = "&results=50") {
  const response = await fetch(baseURL.external + url);
  const users = await response.json(); // convert to JS ready to use object
  const usersLocal = await getUsersFromLocalServer();
  const allUsers = [...convertRandomUserMock(users.results, []), ...usersLocal];

  addTeachersToMain(allUsers);
  addTeachersToFavorites(allUsers);

  addInfoModalToPage();

  return allUsers;
}
export async function getUsersFromServerToStatistics(url, notAddToStatictics) {
  const response = await fetch(baseURL.external + url);
  const users = await response.json();
  const usersLocal = await getUsersFromLocalServer();

  const allUsers = [...convertRandomUserMock(users.results, []), ...usersLocal];
  if (users) {
    if (notAddToStatictics) {
      // used for sorting users
      return allUsers;
    } else {
      addUsersToStatistics(allUsers);
    }
  }
  return allUsers;
}

export async function getUsersFromLocalServer() {
  const response = await fetch(baseURL.local);
  const users = await response.json();
  return users;
}

export async function addUserToLocalServer(user) {
  const response = await fetch(baseURL.local, {
    headers: {
      //for header
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    method: "POST",
    body: JSON.stringify(user),
  });

  const userInJS = await response.json();
  return userInJS;
}
