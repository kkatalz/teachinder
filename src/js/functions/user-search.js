import { getUsersFromServer } from "../services/users.js";

//5th task
export async function getUserByParameter(searchProperty, value) {
  //name, note, age.
  if (
    //instead of it
    // searchProperty === "full_name" ||
    // searchProperty === "age" ||
    // searchProperty === "note"

    //use lodash methods
    _.includes(["full_name", "age", "note"], searchProperty)
  ) {
    return await getUserByAnyParameter(searchProperty, value);
  }
}

async function getUserByAnyParameter(searchProperty, value) {
  let users = await getUsersFromServer();

  if (searchProperty === "age" && _.isString(value)) {
    const ages = value.match(/\d+/g); //[ '10', '25' ]
    if (!ages || ages.length === 0) return;
    if (ages.length === 1) {
      // return users.find((user) => user[searchProperty] === parseInt(value));
      return _.find(users, (user) => user[searchProperty] === parseInt(value));
    }
    const lowLimitAge = parseInt(ages[0]);
    const highLimitAge = parseInt(ages[1]);

    // return users.find(
    //   (user) =>
    //     user[searchProperty] > lowLimitAge &&
    //     user[searchProperty] < highLimitAge
    // );
    return _.find(
      users,
      (user) =>
        user[searchProperty] > lowLimitAge &&
        user[searchProperty] < highLimitAge
    );
  }

  // const searchedUser = users.find((user) =>
  //   typeof value === "string"
  //     ? user[searchProperty]?.includes(value)
  //     : user[searchProperty] === value
  // );

  const searchedUser = _.find(users, (user) =>
    _.isString(value)
      ? _.includes(user[searchProperty], value)
      : user[searchProperty] === value
  );

  return searchedUser;
}
