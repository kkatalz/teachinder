import { convertRandomUserMock } from "./users-convert.js";
import { additionalUsers, randomUserMock } from "../data/FE4U-Lab2-mock.js";
import { getUsersFromServer } from "../services/users.js";
import _ from "/node_modules/lodash-es/lodash.js";

//function for 3rd task

// getFilteredUsers("Germany", "10-25", "male", true)

export async function getFilteredUsers(
  country,
  age,
  gender,
  favorite,
  withPicture
) {
  // const staledUsers = convertRandomUserMock(randomUserMock, additionalUsers);
  // let users = convertRandomUserMock(randomUserMock, additionalUsers);

  const staledUsers = await getUsersFromServer();
  let users = _.cloneDeep(staledUsers);
  //country check

  // function isCountryInUser(user){
  //   if(user.country === country ) return true;
  //   return false
  // }or

  // if (country) users = users.filter((user) => user.country === country);
  if (country) {
    users = _.filter(users, { country: country });
  }

  // let x = ()=>{}

  // (user) => user.country === country
  // JavaScript

  //age check
  // if (age) {
  //   if (typeof age === "string") {
  //     // "10-25"
  //     const ages = age.match(/\d+/g); //[ '10', '25' ]
  //     if (ages.length === 1) {
  //       //age = "-10"
  //       if (age[0] === "-") {
  //         const highLimitAge = parseInt(ages[0]);
  //         users = users.filter((user) => user.age <= highLimitAge);

  //         //age= 10-
  //       } else if (age[0] !== "-") {
  //         const lowLimitAge = parseInt(ages[0]);
  //         users = users.filter((user) => user.age >= lowLimitAge);
  //       }
  //     } else {
  //       const lowLimitAge = parseInt(ages[0]);
  //       const highLimitAge = parseInt(ages[1]);
  //       users = users.filter(
  //         (user) => user.age >= lowLimitAge && user.age <= highLimitAge
  //       );
  //     }
  //   } else {
  //     users = users.filter((user) => user.age === age);
  //   }
  // }

  if (age) {
    if (typeof age === "string") {
      const [lowLimitAge, highLimitAge] = age.split("-").map(_.parseInt);
      users = _.filter(
        users,
        (user) => user.age >= lowLimitAge && user.age <= highLimitAge
      );
    } else {
      users = _.filter(users, { age: age });
    }
  }

  //gender check
  // if (gender && (gender === "female" || gender === "male"))
  //   users = users.filter((user) => user.gender === gender);

  if (gender && (gender === "female" || gender === "male")) {
    users = _.filter(users, { gender: gender });
  }

  //favorite check
  // if (favorite !== null) {
  //   users = staledUsers.filter((user) => user.favorite === favorite);
  // }

  if (!_.isNull(favorite)) {
    users = _.filter(users, { favorite: favorite });
  }

  // if (withPicture !== null) {
  //   if (withPicture) {
  //     users = users.filter((user) => user.picture_large !== null);
  //   } else {
  //     users = staledUsers;
  //   }
  // }

  if (!_.isNull(withPicture)) {
    if (withPicture) {
      users = _.filter(users, (user) => !_.isNull(user.picture_large));
    } else {
      users = _.cloneDeep(staledUsers);
    }
  }

  return users;
}
