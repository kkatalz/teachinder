import { getUsersFromServerToStatistics } from "../services/users.js";

//4th task
export async function getSortedUsers(sortProperty, sortOrder, currentPage) {
  const users = await getUsersFromServerToStatistics(
    `&results=${currentPage * 10}`,
    true
  );

  //became using lodash.
  return _.sortBy(users, [sortProperty], [sortOrder]);
}

// function compareFunction(a, b) {
//   if (b[sortProperty] < a[sortProperty]) {
//     return sortOrder === "asc" ? 1 : -1;
//   }
//   if (a[sortProperty] < b[sortProperty]) {
//     return sortOrder === "asc" ? -1 : 1;
//   }
//   return 0;
// }

// return users.sort(compareFunction);
