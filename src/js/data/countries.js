import { convertRandomUserMock } from "../functions/users-convert.js";
import { additionalUsers, randomUserMock } from "./FE4U-Lab2-mock.js";

let users = convertRandomUserMock(randomUserMock, additionalUsers);

//const countries = users;
//   .map((user) => {
//     return user.country;
//   })
//   .filter((user) => user !== null); // [country1, country2,...]

// export const countriesWithoutDuplicates = Array.from(new Set(countries));

const countries = _.map(users, "country").filter((user) => user !== null);
export const countriesWithoutDuplicates = _.uniq(countries);

// [
//     user1,
//     user2,
//     user3
// ]
// map -> user=>"qwerty"
// [
//     "qwerty",
//     "qwerty",
//     "qwerty"
// ]
